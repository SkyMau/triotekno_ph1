#include <application.h>

class IOPort
{
    public:

        byte file[8];
        byte deviceAddress;

        IOPort(void);
        ~IOPort(void);

        void begin(byte address, byte type);
        byte readFile(void);
        byte readBit(byte position);
        byte clearFile(void);
        byte clearBit(byte position);
        byte writeFile(byte value);
        byte writeBit(byte position);
        byte write(byte position, byte value);

    private:

        byte send(void);
};
