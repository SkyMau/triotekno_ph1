
#include "IOPort.h"
#include "math.h"

IOPort::IOPort(void) {}

IOPort::~IOPort(void) {}

void IOPort::begin(byte address, byte type) //Type 1: Input//Type 0: Output//
{
    deviceAddress = address;
    if (type == 1)
    {
        for (byte i = 0; i < 8; i++) file[i] = 1;
    }
    else
    {
        for (byte i = 0; i < 8; i++) file[i] = 0;
    }
    send();
}

byte IOPort::readFile(void)
{
    byte result = 0;
    for (byte i = 0; i < 8; i++)
    {
        if ((byte)file[i] == 1) result += pow(2, i);
    }
    return result;

    /*byte result = 0;
    Wire.requestFrom(deviceAddress, 1);
    if(Wire.available()) result = Wire.receive();
    return result;*/
}

byte IOPort::readBit(byte position)
{
    byte result = 0;
    if (position >= 0 && position < 8) result = (byte)file[position];
    return result;
}

byte IOPort::writeFile(byte value)
{
    for (int i = 7; i > -1; i--)
    {
        byte subtrahend = pow(2, i);
        if (value >= subtrahend)
        {
            value -= subtrahend;
            file[i] = 1;
        }
        else file[i] = 0;
    }
    return send();
}

byte IOPort::writeBit(byte position)
{
    if (position >= 0 && position < 8)
    {
        if (file[position] == 0) file[position] = 1;
    }
    return send();
}

byte IOPort::write(byte position, byte value)
{
    if (position >= 0 && position < 8)
    {
        if (value == 1 || value == 0) file[position] = value;
    }
    return send();
}

byte IOPort::clearFile(void)
{
    for (byte i = 0; i < 8; i++) file[i] = 0;
    return send();
}

byte IOPort::clearBit(byte position)
{
    if (position >= 0 && position < 8)
    {
        if ((byte)file[position] > 0) file[position] = 0;
    }
    return send();
}

byte IOPort::send(void)
{
    byte result = 0;
    byte tFile = 0;
    for (byte i = 0; i < 8; i++)
    {
        if ((byte)file[i] == 1) tFile += pow (2, i);
    }
    Wire.beginTransmission(deviceAddress);
    Wire.write(tFile);
    result = Wire.endTransmission();
    return result;

    /*
    0: success
    1: data too long to fit in transmit buffer
    2: received NACK on transmit of address
    3: received NACK on transmit of data
    4: other error
    */
}
