/*
* Project Triotekno_Ph1
* Description:
* Author: Mauricio Suarez
* Date: 09/06/2018
*/

#include "dht_lib.h"
#include "IOPort.h"
#include "Collection.h"

#define DHTPIN 2
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

int monitorPort = 9002;
TCPServer server = TCPServer(monitorPort);
TCPClient sclient;

TCPClient client;
byte serverAddress[] = { 192, 168, 100, 2 }; //Endpoint Address

String tInteractor = "5ac0106dea2f2aaa6c000009"; // t
String hInteractor = "5afd045fc63f39eb6400000b"; // h
String o1Interactor = "5b36884b1ca89de81c00000c"; // o1
String o2Interactor = "5b3688611ca89de81c00000d"; // o2

IOPort PORTA;

float lt = -300; //last temperature MSL
float lh = -300; //last humidity MSL

void setup() {
    Serial.begin(9600);
    dht.begin();
    server.begin();
    Wire.begin();
    PORTA.begin(32, 0);

    ////////////////////////////////////////
    bool timeUpdated = false;
    while(!timeUpdated){
        if (client.connect(serverAddress, 9001)) {
            String dtRequest = "time|5ab958c4f4d659300400000c|0|0|";
            dtRequest += WiFi.localIP().toString();
            dtRequest += ":";
            dtRequest += monitorPort;
            dtRequest += "|*";
            Serial.println("Request: " + dtRequest);
            client.print(dtRequest);
            //// Start waiting for response ////
            String response = "";
            unsigned long lastRead = millis();
            while (millis() - lastRead < 3000) {
                if (client.available() > 0) {
                    char c = client.read();
                    if (c == '*') {
                        response += c;
                        Serial.println("Response: " + response);
                        break;
                    }
                    else response += c;
                }
            }
            client.stop();
            if(response != ""){
                String responseType = response.substring(0, response.indexOf('|'));
                String responseData = response.substring(response.indexOf('|') + 1, response.length() - 2);
                Serial.println("Type: " + responseType);
                Serial.println("Data: " + responseData);
                Time.setTime(responseData.toInt());
                timeUpdated = true;
            }
        } else {
            Serial.println("Date-Time Sync failed.");
            delay(3000);
        }
    }
    ////////////////////////////////////////
    server.begin();
}

void loop() {
    float h = dht.getHumidity();
    float t = dht.getTempCelcius();
    // Check if any reads failed and exit early (to try again).
	if (isnan(h) || isnan(t)) {
		Serial.println("Failed to read from DHT sensor!");
		return;
	}
    if (lt != t) {
        lt = t;
        Serial.println("Sending temperature...");
        Collection request;
        request.elements[0] = "log";
        request.elements[1] = tInteractor;
        request.elements[2] = Time.now();
        request.elements[3] = String(t);
        request.elements[4] = "x";
        Serial.println("Request: " + request.ToString('|'));
        if (client.connect(serverAddress, 9001)) {
            client.print(request.ToString('|'));
		} else {
			Serial.println("connection failed");
		}
        //if (client.connected()) client.stop();
    }
    if (lh != h) {
        lh = h;
        Serial.println("Sending humidity...");
        Collection request;
        request.elements[0] = "log";
        request.elements[1] = hInteractor;
        request.elements[2] = Time.now();
        request.elements[3] = String(h);
        request.elements[4] = "x";
        Serial.println("Request: " + request.ToString('|'));
        if (client.connect(serverAddress, 9001)) {
            client.print(request.ToString('|'));
		} else {
			Serial.println("connection failed");
		}
        //if (client.connected()) client.stop();
    }
    ///////////////////////////////////////////////////////////
    if (sclient.connected()) {
        String buffer = "";
        unsigned long lastRead = millis();
        while (millis() - lastRead < 3000) {
            if (sclient.available() > 0) {
                char currentChar = sclient.read();
                if (currentChar == '*') {
                    break;
                } else {
                    buffer += currentChar;
                }
            }
		}
        // action|interactor|time|value|aux|*
        Collection serverReq(buffer, '|');
        buffer = "";
        if (serverReq.elements[0] == "action") {
            if (serverReq.elements[1] == o1Interactor) {
                if (serverReq.elements[3] == "1") PORTA.writeBit(1);
                else PORTA.clearBit(1);
            } else if (serverReq.elements[1] == o2Interactor) {
                if (serverReq.elements[3] == "1") PORTA.writeBit(2);
                else PORTA.clearBit(2);
            }
        }
        sclient.print("ok|0|*");
        sclient.stop();
        buffer = "";
    } else {
        sclient = server.available();
    }
    delay(1000); //////////////////////////////////////////
}
