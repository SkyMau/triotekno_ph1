#include <application.h>

class Collection
{
    public:

        int index;
        bool hasElements;
        String elements[10];

        Collection(String message, char separator);
        Collection();
        ~Collection(void);

        String ToString(char separator);
};
