#include "Collection.h"

Collection::Collection() {}

Collection::Collection(String message, char separator)
{
    hasElements = false;
    index = 0;

	int separatorPosition = 0;

	do
	{
		separatorPosition = message.indexOf(separator);
		if (separatorPosition != -1)
		{
		    String tString1 = message.substring(0, separatorPosition);
			elements[index] = tString1;
			//Serial.println(message.substring(0, separatorPosition));
			String tString2 = message.substring(separatorPosition + 1, message.length());
			message = tString2;
			index++;
		}
		else
		{  // here after the last separator is found
			if (message.length() > 0)
			//Serial.println(message);  // if there is text after the last separator, add it
			elements[index] = message;
		}
	}
	while(separatorPosition >= 0);

	if (index > 0) hasElements = true;
}

Collection::~Collection(void) {}

String Collection::ToString(char separator)
{
    int index = 0;
    String result = "";
    while(index < sizeof(elements))
    {
        result += elements[index];
        result += separator;
        index++;
        if (elements[index] == "") break;
    }
    result += "*";
    return result;
}
